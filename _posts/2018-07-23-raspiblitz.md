---
layout: post
title: "RaspiBlitz"
image: images/rpi-mbp.gif
categories:
  - hardware
tags:
  - rpi
  - btc
---
# BTC Lightning Node on Raspberry Pi

{% raw %}<img src="/images/rpi-mbp.jpg" alt="rpi-mbp-8bit">{% endraw %}

**RaspiBlitz** (by @rootzoll) is the fastest and cheapest way to get your own Lightning Node running on a RaspberryPi with a nice LCD.

Use [this](https://github.com/rootzoll/raspiblitz/blob/master/README.md#hardware-needed) guide to build and spin up your node.

If you confident in your wifi network - use `sudo raspi-config` to setup wireless connection ([ref](https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md)).

[RaspiBlitz on GitHub](https://github.com/rootzoll/raspiblitz).
