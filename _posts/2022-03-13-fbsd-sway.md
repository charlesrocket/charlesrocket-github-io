---
layout: post
title: "Sway + FreeBSD"
image: images/fbsd.png
categories:
  - software
tags:
  - freebsd
  - sway
---
# Sway and FreeBSD

{% raw %}<img src="https://raw.githubusercontent.com/charlesrocket/freebsd-station/master/screenshot.gif" alt="Screenshot (repo)">{% endraw %}

I started to migrate from macOS back in December 2020 - `kext` "deprecation" was the [last](https://twitter.com/patrickwardle/status/1318437929497235457) drop. And we won, [eventually](https://twitter.com/patrickwardle/status/1349488392732491776). But the fact that you had to fight for it, and the internal reaction - everything now is just screaming theres nothing left of what used to be **OSX**. Not to mention no macOS Server and no Pro. That is it. So...

{% raw %}<img src="/images/fbsd-scrn3.gif" alt="Screenshot">{% endraw %}

Early `FBSD13B1` builds on my laptop were not as smooth as I wanted - you had to tweak CPU, network, boot, peripheral drivers, etc. Fun but not as efficient as it could be. A few months later in the final `FBSD13` the difference was overcoming. I mean touchscreen-out-of-the-box overcoming. Then `sway`'s stability improved dramatically and I started working on my [freebsd-station](https://github.com/charlesrocket/freebsd-station) playbook. After another few months, I managed to polish the layout and started porting the environment. I indeed forgot that I don't need minutes for the IDE to load. No more web-based Atom and parasitic `brew`. macOS would still be a better option (machine) for `Capture One` tasks, but it's not like I will be building a medium format rig. So I can drop the mouse as well.

{% raw %}<img src="/images/fbsd-scrn1.gif" alt="Screenshot">{% endraw %}
{% raw %}<img src="/images/fbsd-scrn2.gif" alt="Screenshot">{% endraw %}

After porting 100% of my macOS environment to FreeBSD, I still need to close some gaps in media. But to achieve 'full automation dandy', I would need my [freebsd-server](https://github.com/charlesrocket/freebsd-server) with `poudriere` deployed and full of configs.
